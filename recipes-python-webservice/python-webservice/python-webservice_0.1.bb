DESCRIPTION = "Python flask restful webservice"
SECTION = "examples"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PR = "r0"
SRCREV="${AUTOREV}"
SRC_URI += "git://gitlab.com/anand.s/rpi-restful-webservice-python.git;protocol=https;branch=master"

SRC_URI[md5sum] = "886eab56b8b4f87c35a6d1d90e83d5d7"
SRC_URI[sha256sum] = "6f8394f03984c42077285040d6f05bcf2b6d410c06a80ef73add45c44c708eb9"

S = "${WORKDIR}/git"
inherit autotools

do_install() {
	     install -d ${D}${bindir}/python-webservice
             install -d ${D}${bindir}/python-webservice/templates
	     install -m 0755 ${S}/database.py ${D}${bindir}/python-webservice
             install -m 0755 ${S}/pythonrestfulAPI.py ${D}${bindir}/python-webservice
             install -m 0755 ${S}/templates/display_cpuinfo.html ${D}${bindir}/python-webservice/templates
             install -m 0755 ${S}/templates/employee.html ${D}${bindir}/python-webservice/templates
             install -m 0755 ${S}/templates/index.html ${D}${bindir}/python-webservice/templates
             install -m 0755 ${S}/templates/list.html ${D}${bindir}/python-webservice/templates
	     install -m 0755 ${S}/templates/result.html ${D}${bindir}/python-webservice/templates
	     install -m 0755 ${S}/templates/update.html ${D}${bindir}/python-webservice/templates
}
