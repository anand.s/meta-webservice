# Base this image on core-image-minimal
include recipes-core/images/rpi-basic-image.bb
LICENSE = "MIT"

IMAGE_CMD_rpi-sdimg[vardepsexclude] = "DATETIME"
REQUIRED_DISTRO_FEATURES = "x11"
IMAGE_INSTALL_append = "gtk+3 openjdk-8 python sqlite3 python-sqlite3 python-flask python-pip firefox packagegroup-core-x11-base python-webservice java-webservice json-c curl"
